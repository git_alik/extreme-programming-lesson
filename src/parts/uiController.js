import devisionController from './devisionController';
import multiplyController from './multiplyController';

let first;
let second;
let action;
let result;

export default {
  setFirst(arg) {
    first = arg;
  },
  setSecond(arg) {
    second = arg;
  },
  setAction(arg) {
    action = arg;
  },
  getResult() {
    if (action === '*') {
      multiplyController.setFirst(first);
      multiplyController.setSecond(second);
      result = multiplyController.count();
    }
    if (action === ':') {
      devisionController.setFirst(first);
      devisionController.setSecond(second);
      result = devisionController.count();
    }
    return result;
  },
};
