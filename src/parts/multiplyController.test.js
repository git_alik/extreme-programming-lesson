import test from 'ava';
import sinon from 'sinon';
import multiplyController from './multiplyController';

test('should be a number', (t) => {
  multiplyController.setFirst(3);
  multiplyController.setSecond(4);
  const result = multiplyController.count();
  t.is(result, 12);
});
