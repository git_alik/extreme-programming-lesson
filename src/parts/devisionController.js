let first;
let second;

export default {
  setFirst(arg) {
    first = arg;
  },
  setSecond(arg) {
    second = arg;
  },
  count() {
    return first / second;
  }
};
